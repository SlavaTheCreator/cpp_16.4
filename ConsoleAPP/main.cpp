#include <iostream>
#include <time.h>
int getDay(){
    struct tm buf;
    time_t t = time(NULL);
    localtime_r(&t, &buf);
    return buf.tm_mday;
}

int rowSum(int row[], int size){
    int sum = 0;

    for (int i = 0; i < size; ++i) {
        sum += row[i];
    }

    return sum;
}

int main(int argc, const char * argv[]) {
    const int size = 5;
    int idx = getDay() / size;
    int array[size][size];

    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            array[i][j] = i+j;
            std::cout << array[i][j] << ' ';
        }
        std::cout << '\n';
    }

    size_t size_ {std::size(array[idx])};
    int sum = rowSum(array[idx], size_);
    std::cout << "Sum = " << sum;
}